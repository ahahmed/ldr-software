from FSAstateautomation import supervisor_scheduler # import FSA
from datetime import time
import time
import datetime
import os
import csv

import sys
import time
import shutil
import psutil
import logging
from datetime import datetime
import logging
import shutil
from shutil import copytree
#from __future__ import print_function

#-------------------------------------------------------------------------------
# LDR FSA supervisor script
# Author: Ahmed Abdirashid
#-------------------------------------------------------------------------------
recycle =  'python powercontrol.py'
# Create directory called  00_TEST_INFO in current directory
directory = '00_TEST_INFO'
if not os.path.exists(directory):
    os.makedirs(directory)

# Create subdirectory '00_TEST_INFO/radiation_doses'
directory = '00_TEST_INFO/transitions_log'
#directory_full = '00_TEST_INFO'+ '/' +'Chip_Characterization'
if not os.path.exists(directory):
    os.makedirs(directory)
timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")

# Current directory
projectdir = os.getcwd()

#Logginf transitions into a file
transitions_log = directory + '/' +"Brain_Function_Logging.txt"


def restart_program():
    """Restarts the supervusor script, with file objects and descriptors
       cleanup
    """
    try:
        p = psutil.Process(os.getpid())
        for handler in p.   open_files() + p.connections():
            os.close(handler.fd)
    except Exception, e:
        logging.error(e)
    python = sys.executable
    os.execl(python, python, *sys.argv)


    #now = datetime.datetime.now().strftime("%I:%M%p")
    # print(now)     # print("CPU  = ", psutil.cpu_percent())
    # print("Memory = ", psutil.swap_memory())
    # print ("start")
timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
Full_charac_time_begin = "01:00"
Full_charac_time_end =  "06:00"
power_state_timout = 1 # 1 is 42 seconds



def powerStateHanlder():
    #################################################################################
    # Power state handler
    # - run Ring oscillators - without generating data
    # - run temperature sensors -  without generating data
    ################################################################################
    logging.info("Entering power state")
    #shutil.copytree(directory_full, '/home/lowdoserate4/cernbox3/ldrtest' + '/Full_characterization_Backup' + timestamp)
    try:
        if os.path.isfile(projectdir + "/" +"Ldr_Temp_no_output.py") and os.path.isfile(projectdir + "/" +"scan_ring_oscillators_no_output.py") and os.path.isfile(projectdir + "/" +"scan_analog_no_output.py"):
            logging.info("Power state files are included"   )
            TempCMD = "python Ldr_Temp_no_output.py"
	    analogScan         = "python scan_analog_no_output.py"
            RingOScillatorCMD  = 'python scan_ring_oscillators_no_output.py'
            recycle             =  'python powercontrol.py' 
        else:
        	print "Files are missing in current dir"
        	print os.system("pwd")
        	sys.exit()
        logging.info('Power state is staring...')

        for i in range(power_state_timout):
            os.system(recycle)
            os.system(analogScan)
            if os.path.exists(projectdir + "/" + "output_data"):
                try:
                    os.system("rm -r output_data")
                except Exception as e:
                    raise "folder does not exist"
                    pass
            #os.system(TempCMD)
            os.system(RingOScillatorCMD)
    except Exception as e:
        logging.error("Exception occurred", exc_info=True)

    newState = "HalfCharac_state"
    if time.strftime('%H:%M') >= Full_charac_time_begin:
        if time.strftime('%H:%M') <= Full_charac_time_end:
            newState = "FullCharac_state"
    return (newState)


#@profile
def halfCharacHanlder():
    #################################################################################
    # HalfCharac-state state handler
    # - run Ring oscillators
    # - run temperature sensors
    #################################################################################
    print ("Entering Half Characterization state")
    Mdata =[["HalfCharac-state: ", time.strftime("%d_%m_%Y_%H_%M_%S")]]
    file = open(transitions_log, 'a')
    with file:
        writer = csv.writer(file)
        writer.writerows(Mdata)
        file.close()

    print("halfCharac-state")
    try:
    	        ###### backup #####
        #os.system('./pmonitor.sh')
        #shutil.move("backup_LDR.tar.gz", "/run/media/lowdoserate4/B4F6-4122/")
        os.system('python temp_rad_monitor.py ')
        os.system('python scan_ring_oscillators.py')
        os.system('python LdrRingplotFreq.py')

    except Exception as e:
        logging.error("Exception occurred", exc_info=True)


    if time.strftime('%H:%M') >= Full_charac_time_begin:
        if time.strftime('%H:%M') <= Full_charac_time_end:
            newState = "FullCharac_state"
            return (newState)
    logging.info('restarting the script')
    restart_program()

#################################################################################
# HalfCharac-state state handler
# -
#################################################################################
def fullCharacHanlder():
    print ("Entering Full Characterization state")


    Mdata =[
          ["FullCharac-state: ", time.strftime("%d_%m_%Y_%H_%M_%S") ]
           ]
    file = open(transitions_log, 'a')
    with file:
        writer = csv.writer(file)
        writer.writerows(Mdata)
        file.close()
    try:
    	os.system(recycle)
    	os.system('python temp_rad_monitor.py')
        os.system('python scan_ring_oscillators.py')
        os.system('python LdrRingplotFreq.py')
        os.system('python Full_characterization.py')
        os.system(recycle)
        ###### backup #####
        #shutil.move("backup.tar.gz", "/run/media/lowdoserate4")
    except Exception as e:
        logging.error("Exception occurred", exc_info=True)
    
    restart_program()


#@profile
def ErrorHanlder(state):
## TO be done
    return ("error_state")





#################################################################################
# Main program
#################################################################################

if __name__== "__main__":
    # os.system("export PATH=$HOME/miniconda/bin:$PATH")
    # os.system("cd bdaq53")
    # os.system("python setup.py develop")
    # os.system("cd ..")
    #--------------------------------------------------
    FSA = supervisor_scheduler()
    # Create a power state with its own state handler
    FSA.include_state("Power_state", powerStateHanlder)
    # Create a half_characterization state with its own state handler
    FSA.include_state("HalfCharac_state", halfCharacHanlder)
    # Create a Full_characterization state with its own state handler
    FSA.include_state("FullCharac_state", fullCharacHanlder)
    # Create error state without
    FSA.include_state("Error_state", ErrorHanlder)
    #Begin with power state when ever the the script is executed
    FSA.ILDE_entry("Power_state")
    #--------------------------------------------------------
    #Begin the FSA
    FSA.begin_supervisors()

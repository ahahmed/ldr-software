
import numpy
import yaml
import time
import os
import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils
from bitarray import bitarray
from datetime import datetime
import matplotlib
import matplotlib.pyplot as plt
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv


chip_configuration = 'default_chip.yaml'


local_configuration = {
    # Hardware settings
    'VDDA'              : 1.27,
    'VDDD'              : 1.27,
    'VDDA_cur_lim'      : 0.9,
    'VDDD_cur_lim'      : 0.9,
}   


class scan_ring_oscillator(ScanBase):
    scan_id = "scan_ring_oscillator"


    
    def configure(self, **kwargs):
        super(scan_ring_oscillator, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
    
    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2  
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)


    def scan(self,  **kwargs):

	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)

			self.logger.info("RO Oscillation ")


if __name__ == "__main__":

    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = scan_ring_oscillator()
    scan.start(**configuration)

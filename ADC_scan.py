#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Wafer probing script to probe a full wafer
'''
import tables as tb
from tqdm import tqdm
import sys, os, time
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com
from datetime import datetime
import logging
import yaml
import time

import socket
import sys
import time
import re
import os
import math
import os
import time
import yaml
import logging
import copy
import numpy as np

from basil.dut import Dut

local_configuration = {
    # General settings
    'wafer_id': '',   # Wafer number from the TWiki list, in decimal
    'n_chips': 89,

    # Powering settings
    'VAUX1': 1.2,   # Default VDD_PLL
    'VAUX2': 1.2,   # Default VDD_CML
    'LDO_VINA': 1.7,    # Default analog voltage in LDO mode
    'LDO_VIND': 1.7,    # Default digital voltage in LDO mode
    'LDO_IINA': 0.7,    # Analog current limit in LDO mode
    'LDO_IIND': 0.7,    # Digital current limit in LDO mode
    'SHUNT_VINA': 2,    # Analog voltage limit in Shunt mode
    'SHUNT_VIND': 2,    # Digital voltage limit in Shunt mode
    'SHUNT_IINA': 0.6,  # Default analog working point in Shunt mode
    'SHUNT_IIND': 0.6,  # Default digital working point in Shunt mode


    # Analog measurements to be conducted using the external MUX
    'analog_measurements': [
        'VINA',
        'VIND',
        'VDDA',
        'VDDD',
        'VREF_ADC_OUT',
        'GND',
        'SLDO_VREFA',
        'SLDO_VREFD'
    ],

    # Analog measurements to be conducted using the internal MUX
    'internal_mux_measurements': {
        'TEMPSENS_1': 15,
        'TEMPSENS_2': 17,
        'TEMPSENS_3': 27,
        'TEMPSENS_4': 19,
        'VIN Ana. ShuLDO': 23,
        'VOUT Ana. ShuLDO': 24,
        'VREF Ana. ShuLDO': 25,
        'VOFF Ana. ShuLDO': 26,
        'VIN Dig. ShuLDO': 29,
        'VOUT Dig. ShuLDO': 30,
        'VREF Dig. ShuLDO': 31,
        'VOFF Dig. ShuLDO': 32
    },
}

V_refADC = 0.900 #[V] #
r = 10000
LSB = float(V_refADC/math.pow(2, 12)) #

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}
# voltage_mux_array=['ADCbandgap','CAL_MED_left','CAL_HI_left','TEMPSENS_1','RADSENS_1','TEMPSENS_2','RADSENS_2','TEMPSENS_4','RADSENS_4','VREF_VDAC','VOUT_BG','IMUX_out','VCAL_MED','VCAL_HIGH','RADSENS_3','TEMPSENS_3','REF_KRUM_LIN','Vthreshold_LIN','VTH_SYNC','VBL_SYNC','VREF_KRUM_SYNC','VTH_HI_DIFF','VTH_LO_DIFF','VIN_Ana_SLDO','VOUT_Ana_SLDO','VREF_Ana_SLDO','VOFF_Ana_SLDO','ground','ground1','VIN_Dig_SLDO','VOUT_Dig_SLDO','VREF_Dig_SLDO','VOFF_Dig_SLDO','ground2']
voltage_mux_array=['ADCbandgap','CAL_MED_left','CAL_HI_left','TEMPSENS_1','RADSENS_1','TEMPSENS_2','RADSENS_2','TEMPSENS_4','RADSENS_4','VOUT_BG','IMUX_out','VCAL_MED','VCAL_HIGH','RADSENS_3','TEMPSENS_3','REF_KRUM_LIN','Vthreshold_LIN','VTH_SYNC','VBL_SYNC','VREF_KRUM_SYNC','VTH_HI_DIFF','VTH_LO_DIFF','VIN_Ana_SLDO','VOUT_Ana_SLDO','VREF_Ana_SLDO','VOFF_Ana_SLDO','ground','ground1','VIN_Dig_SLDO','VOUT_Dig_SLDO','VREF_Dig_SLDO','VOFF_Dig_SLDO','ground2']
# voltage_mux_array=['VOUT_Ana_SLDO', 'VOUT_Dig_SLDO', 'ADCbandgap']

current_mux_array=['Iref','IBIASP1_SYNC','IBIASP2_SYNC','IBIAS_DISC_SYNC','IBIAS_SF_SYNC','ICTRL_SYNCT_SYNC','IBIAS_KRUM_SYNC','COMP_LIN','FC_BIAS_LIN','KRUM_CURR_LIN','LDAC_LIN','PA_IN_BIAS_LIN','COMP_DIFF','PRECOMP_DIFF','FOL_DIFF','PRMP_DIFF','LCC_DIFF','VFF_DIFF','VTH1_DIFF','VTH2_DIFF','CDR_CP_IBIAS','VCO_BUFF_BIAS','VCO_IBIAS','CML_TAP_BIAS0','CML_TAP_BIAS1','CML_TAP_BIAS2']





class ADC_scan(object):

    def __init__(self, conf='default_chip.yaml'):
        self.config_file = conf
        self.logger = logging.getLogger('test')
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)

        #clearos.system('sudo chmod 777 /dev/ttyUSB0')
        #os.system('sudo chmod 777 /dev/gpib0')
        self.init()
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        #self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        #self.chip.write_register(register='MONITOR_CONFIG', data=0b110000000101, write=True)
 #       self.set_monitor()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array

    def init(self):
        devices = Dut('keithley2410LDR.yaml')
        devices.init()
        self.smu = devices['Sourcemeter']

    def measure_voltage(self):
        #self.smu.set_current(0)
        #self.smu.source_current()
        try:
            # self.smu.on()
            #self.needle_card.adc_mux(mux)
            # value = self.smu.get_voltage()
            #value = self.smu.get_voltage().split(',')[0]
            value = self.smu.get_voltage().split(',')[0]
        except ValueError as e:
            self.logger.exception(e)
            value = 0.0
        #self.needle_card.adc_mux('GND')
        #input('Turn it off')
        self.smu.off()
        time.sleep(0.1)
        return float(value)


    def get_chip_status(self, timeout=1000):
        '''
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        '''

        voltages_interal = []
        currents_interal = []
        voltages_external = []
        currents_external = []

        self.logger.info('Recording chip status...')
        n = 1
        try:
            self.chip.enable_monitor_filter()
            self.chip.enable_monitor_data()

            for vmonitor in np.arange(22, 33, 1):
                self.chip.get_ADC(typ='U', address=vmonitor)
                name = self.chip.voltage_mux[vmonitor]
                # voltages_external.append(self.measure_voltage())
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        voltages_interal.append(LSB*(userk_data['Data'][0]))
                        voltages_external.append(self.measure_voltage())
                        print("internal")
                        print voltages_interal
                        print("external")
                        print voltages_external
                        raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')

            for cmonitor in np.arange(0, 26, 1):
                self.chip.get_ADC(typ='I', address=cmonitor)
                name = self.chip.current_mux[cmonitor]
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        currents_interal.append((LSB*(userk_data['Data'][0])))
                        currents_external.append(self.measure_voltage())
                        print currents_interal
                        raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')
        except:
            self.logger.error('There was an error while receiving the chip status.')
        self.chip._reset_ADC()
        return voltages_interal, currents_interal, voltages_external, currents_external



    def main(self, **kwargs):
        #for x in range(1):
        voltages_interal, currents_interal, voltages_external, currents_external = self.get_chip_status()
        print("_____voltages_interal________")
        print voltages_interal
        print("_____currents_interal________")
        print currents_interal
        print("_____voltages_external________")
        print voltages_external
        print("______currents_external_______")
        print currents_external
    	# ADC_I_list = []
    	# ADC_V_list = []

        #os.system('sudo chmod 777 /dev/ttyUSB0')
        #os.system('sudo ifconfig enp0s20f0u2 192.168.10.2')
        # Preparations
        #This I only do it to initialize the ADC, cause sometimes the first value given is 'None' and I don't solve it...
	# self.get_adc_value('ADCbandgap', 'U')
    #     for mux_selection in voltage_mux_array:
    #         ADC_V_internal =  LSB*float((self.get_adc_value(mux_selection, 'U')))
    #         time.sleep(0.1)
    #         ADC_V_external =  self.measure_voltage()
    #
    #         print "internal: "
    #         print ADC_V_internal
    #         print "external: "
    #         print ADC_V_external
    #         raw_input("pause")
    #         ADC_V_list.append(ADC_V_internal)
        # time.sleep(1)
        # self.get_adc_value('TEMPSENS_3', 'U')
        # for mux_selection in current_mux_array:
        # for mux_selection in xrange(20):
            # time.sleep(0.1)
            #
    	    # ADC_I_internal = self.get_adc_value(mux_selection, 'I')
            # print "code: "
            # print self.chip.get_chip_status()
            # print ADC_I_internal
            # ADC_I_internal = float(ADC_I_internal)*LSB
    	    # ADC_I_external =  self.measure_voltage()
	    # print ADC_I_internal, ADC_V_external
            # print mux_selection
            # print "internal: "
            # print ADC_I_internal
            # print "external: "
            # print ADC_I_external
            # print "___________________________"
            # raw_input("pause")
	#     ADC_I_list.append(ADC_I_internal)
	#     #print ADC_I
    #
	# ADC_ext = self.get_adc_external_signal_value('U')

        #
        # print ADC_V_list
        # print ADC_I_list




if __name__ == '__main__':
    ADC_scan = ADC_scan('default_chip.yaml')
    ADC_scan.main(**local_configuration)

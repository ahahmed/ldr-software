from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_readout import Temperature_readout
import matplotlib.dates as mdate
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator ,WeekdayLocator ,DateFormatter
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
#from bdaq53.serial_com import serial_com
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import pytemperature
import time
import os
import datetime
import sys
import calendar
import numpy as np
import numpy
import logging
import yaml
import time
import re
import csv
import math
from matplotlib import pyplot as plt





#Global
raw_adc = []
averages = []
Sensors  = []
Filterout_numbers = [1,2,3,4]# for filtering out umwanted char
SenseArraysize = 16

def DeltaV_temperature():
	#TempLDR_instance = Temperature_readout()
	raw_adc = TempLDR_instance.scan()
	raw_adc_buffer = []
	raw_adc_buffer.append(re.findall(r'-?\d+\.?\d*', raw_adc)); raw_adc_buffer = raw_adc_buffer[0] # remove letter
	raw_adc_buffer = map(int, raw_adc_buffer) # find average of alle the ADC for sens 1
	for i in raw_adc_buffer:
		if i in Filterout_numbers:
			raw_adc_buffer.remove(i) # remove all 1,2,3,4

	chunks = [raw_adc_buffer[x:x+16] for x in xrange(0, len(raw_adc_buffer), 16)] # Devide the raw adc values to chunks of 16. Each sensor consis of 32 adc values
	for sublist in chunks: # avarage for the dynamic element method, length of the list = 16
		averages.append(sum(sublist)/float(len(sublist)))
		k = 0
		j = 1
	for i in range(len(averages)-1):# finding DeltaV for every sensor, lenth of the list = 8
		while k|j < len(averages):
			Sensors.append(averages[j]-averages[k])
			k = k+2
			j = j+2
	return Sensors

def meas_temperature():
	rd53a_chip = RD53A()
	rd53a_chip.init()                                                           # Initialize chip
	NTC_temperature_forCal = rd53a_chip._measure_temperature_ntc_CERNFMC( )
	rd53a_chip.close()
        return NTC_temperature_forCal




if __name__ == "__main__":
	ntc = meas_temperature()
	TempLDR_instance = Temperature_readout() # create an instance of class Temperature_readout created by Mohsine
	sensors = DeltaV_temperature()

	sensors.insert(0,ntc)
	sensors.insert(0, time.time())
	# print sensors
	V_refADC_SCC203 = 0.911 #[V]
	LSB = float(V_refADC_SCC203/math.pow(2, 12))


#############################################
# Saving Data
#############################################
	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	directory = '00_TEST_INFO/temperature_sensor/processed_data'
	if not os.path.exists(directory):
    		os.makedirs(directory)
	#out_T_F = open(directory + '/' + "_temperature_sensor-test.txt")
	out_T_F_W = open(directory +'/' + "_temperature_sensor.txt", "a")
	out_T_F_W.write(', '.join(map(repr, sensors)) +	'\n')
	out_T_F_W.close()

	directory = '00_TEST_INFO/temperature_sensor/processed_data'


	# Read RD53a temperature from File  (Delta-code data)
	out_T_F_R = (directory +'/' + "_temperature_sensor.txt")
	time      = []
	NTC  	  = []
	tempsens1 = []
	tempsens2 = []
	tempsens3 = []
	tempsens4 = []
	radsens1  = []
	radsens2  = []
	radsens3  = []
	radsens4  = []
	# T1 = []
	# T2 = []
	# T3 = []
	# T4 = []
	# R1 = []
	# R2 = []
	# R3 = []
	# R4 = []

	#out_pdf = (directory +'/' + "_temperature_sensor.pdf")
	with open(out_T_F_R, 'r') as file_in:
		reader = csv.reader(file_in, delimiter=',')
		for row in reader:
			time.append(row[0])
			NTC.append(row[1])
			tempsens1.append(float(row[2]))
			tempsens2.append(float(row[3]))
			tempsens3.append(float(row[4]))
			tempsens4.append(float(row[5]))
			radsens1.append(float(row[6]))
			radsens2.append(float(row[7]))
			radsens3.append(float(row[8]))
			radsens4.append(float(row[9]))
	time = map(float, time)
	NTC = map(float, NTC)


	tempsens1 = map(float, tempsens1)
	tempsens2 = map(float, tempsens2)
	tempsens3 = map(float, tempsens3)
	tempsens4 = map(float, tempsens4)
	radsens1 = map(float, radsens1)
	radsens2 = map(float, radsens2)
	radsens3 = map(float, radsens3)
	radsens4 = map(float, radsens4)


	directory = '00_TEST_INFO/temperature_sensor/Calibration_Nf_f'
	out_T_C_R = (directory +'/' + "_Nf-data.txt")
	with open(out_T_C_R, 'r') as file_in:
		reader = csv.reader(file_in, delimiter=',')
		for row in reader:
			T1 = float(row[0])
			T2  = float(row[1])
			T3 = float(row[2])
			T4 = float(row[3])
			R1 = float(row[4])
			R2 = float(row[5])
			R3 = float(row[6])
			R4 = float(row[7])


	#print time, tempsens1, tempsens2

	Datetime = []
	for val in time:
		Datetime.append(mdate.epoch2num(val))
	#plotting

	T1 = 4285.2/T1
	T2 = 4285.2/T2
	T3 = 4285.2/T3
	T4 = 4285.2/T4
	R1 = 4285.2/R1
	R2 = 4285.2/R2
	R3 = 4285.2/R3
	R4 = 4285.2/R4


	tempsens1 = np.array((pytemperature.k2c(T1*LSB*np.array(tempsens1))))
	tempsens2 = (pytemperature.k2c(T2*LSB*np.array(tempsens2)))
	tempsens3 = (pytemperature.k2c(T3*LSB*np.array(tempsens3)))
	tempsens4 = (pytemperature.k2c(T4*LSB*np.array(tempsens4)))
	radsens1 = (pytemperature.k2c(R1*LSB*np.array(radsens1)))
	radsens2 = (pytemperature.k2c(R2*LSB*np.array(radsens2)))
	radsens3 = (pytemperature.k2c(R3*LSB*np.array(radsens3)))
	radsens4 = (pytemperature.k2c(R4*LSB*np.array(radsens4)))
	# tempsens1 = np.convolve(tempsens1, np.ones((2))/2, mode='valid')

	# plotting

	#ax1 = fig.add_subplot(111)
	Hour = HourLocator()   # every year
	day  = DayLocator()  # every month
	week = WeekdayLocator()
	directory = '00_TEST_INFO/temperature_sensor/processed_data'
	if not os.path.exists(directory):
	   		os.makedirs(directory)
	#out_T_F = open(directory + '/' + "_temperature_sensor-test.txt")
	out_T_F_W = open(directory +'/' + "temperaturein_C.txt", "a")
	#out_T_F_W = open(directory +'/' + "_Nf-data.txt", "w")
	out_T_F_W.write(str(tempsens1)+ ",")
	out_T_F_W.write(str(tempsens2 )+ ",")
	out_T_F_W.write(str(tempsens3 )+ ",")
	out_T_F_W.write(str(tempsens4 )+ ",")
	out_T_F_W.write(str(radsens1 )+ ",")
	out_T_F_W.write(str(radsens2 )+ ",")
	out_T_F_W.write(str(radsens3 )+ ",")
	out_T_F_W.write(str(radsens4 ))

	temprad = [tempsens1,tempsens2,tempsens3,tempsens4,radsens1,radsens2,radsens3,radsens4 ]


	legendtemp = ['tempsens1','tempsens2','tempsens3','tempsens4','radsens1','radsens2','radsens3','radsens4']
	#figure(figsize=(15, 8))
	fig, (ax1) = plt.subplots( figsize=(15, 8))
	for i in range(len(temprad)):
			ax1.plot_date(Datetime,temprad[i],'o',label = legendtemp[i])#, label = legend[i],s=0.5, color = line_color[i])
	ax1.plot_date(Datetime,NTC,'k-',label = "NTC", linewidth = 4.0) #, label = legend[i],s=0.5, color = line_color[i])


	# xtick format string
	date_fmt = '%d-%m-%y-%H-%M-%S'
	# Use a DateFormatter to set the data to the correct format.
	date_formatter = mdate.DateFormatter(date_fmt)
	ax1.xaxis.set_major_formatter(date_formatter)
	# tick labels diagonal so they fit easier.
	#ax1.autofmt_xdate()
	ax1.set_ylim((25,45))

	try:
		ax1.xaxis.set_major_locator(day)
		ax1.xaxis.set_minor_locator(Hour)
	except Exception as e:
		print('Change the locator day instead')
	finally:
		ax1.xaxis.set_minor_locator(day)


	ax1.autoscale_view()
	fig.autofmt_xdate()

# #	ax2.autofmt_xdate()
# 	ax2.set_ylim((20,40))
# 	ax2.xaxis.set_major_locator(week)
# 	ax2.xaxis.set_minor_locator(day)
# 	ax2.autoscale_view()

	ax1.legend(loc=4)
	ax1.set_title('All 8 temp/rad sensors|  LDO-power | Calibration temp: NTC = 30C ')
	ax1.set_xlabel('Time [day]')
	ax1.set_ylabel('Temperature (C)')

	fig.subplots_adjust(hspace=0)
	#plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
	plt.grid(b=None, which='minor', axis='both')
	plt.savefig(directory +'/' + "AllSensor_last_update.PNG", dpi=1000)

	plt.show()
	# plt.show(block=False)
	# plt.pause(4)
	# plt.close()



	#ax1.gridon()

	# plt.grid(b=None, which='major', axis='both')
	# plt.savefig(directory +'/' + "AllSensor_last_update.PNG", format='PNG')

	# pdf = PdfPages(out_pdf)

	# pdf.savefig(fig)

	# pdf.close()

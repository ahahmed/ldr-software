#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a specified charge into
    enabled pixels to test the analog front-end.
'''

from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',
    'mask_diff': False,

    # DAC settings
    'VCAL_MED': 500,
    'VCAL_HIGH': 1300
}


class AnalogScan(ScanBase):
    scan_id = "analog_scan"

    def configure(self, VCAL_MED=1000, VCAL_HIGH=4000, **kwargs):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100, **kwargs):
        '''
        Analog scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data), unit=' Mask steps')
        with self.readout():
            for mask in mask_data:
                self.chip.write_command(mask['command'])
                if mask['flavor'] == 0:
                    self.chip.write_global_pulse(width=9, write=True)
                    self.chip.write_sync_01(write=False) * 80
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                else:
                    self.chip.inject_analog_single(repetitions=n_injections)
                pbar.update(1)

        pbar.close()
        self.logger.success('Scan finished')

#    def analyze(self, create_pdf=True):
#        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
#            a.analyze_data()
#
#        if create_pdf:
#            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
#                p.create_standard_plots()


if __name__ == "__main__":
    scan = AnalogScan()
    scan.start(**local_configuration)
    scan.close()

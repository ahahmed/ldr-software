from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com

import time
import os
import datetime
import sys
import calendar

import logging
import yaml
import time

chip_configuration = 'default_chip.yaml'
##### monitor settings
##### default sensor config to 000000
d_config = 0

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}
class Temperature_readout():
    scan_id = "temperature_sensor_scan"

    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
 #       self.set_monitor()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array

    def set_monitor(self, d_config=0):

########       set SENSOR_CONFIG_X default values - the same config for the 4 sensors
        self.chip.write_register(register='SENSOR_CONFIG_0', data=(d_config << 6|d_config), write=True)
        self.chip.write_register(register='SENSOR_CONFIG_1', data=(d_config << 6|d_config), write=True)


    def get_temperature(self,sensor):

        temperatures={}
        try:
            timeout=10000

            self.chip.get_ADC(typ='U' ,address=sensor)

            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        temperatures[sensor] = userk_data['Data'][0]
                        return temperatures
            else:
                logging.error('Timeout while waiting for chip Temperature.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
	# globalTime = sys.argv[1]
	# dose = sys.argv[2]

	SENS_CONF_L  = 0b100000
	SENS_CONF_H  = 0b100001
        config_array=[SENS_CONF_L,SENS_CONF_H]
	s_config = 0
	DEM = 0
	CONFIG = 0
########       set MON_ENABLE = 1 (MONITOR_CONFIG[11])
########       set MON_BG_TRIM=10000 (MONITOR_CONFIG[10:6])
########       set MON_ADC_TRIM = 000101 (MONITOR_CONFIG[5:0])
        self.chip.write_register(register='MONITOR_CONFIG', data=0b110000000101, write=True)


	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	directory = '00_TEST_INFO/temperature_sensor'
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/' + timestamp + "_temperature_sensor.txt", "a")

		#tobewritten = "%Timestamp   T_NTC(C) GlobalTime(s)  DoseRange(Mrad)\n"
		#out_T.write(tobewritten)
		out_T.close()

	directory_textfile = directory + '/' + timestamp + "_temperature_sensor.txt"
	directory_notextfile = directory
	out_T = open( directory + '/' + timestamp + "_temperature_sensor.txt", "a")
	now = datetime.datetime.now()
        out_T = open(directory_textfile, "a+")

	print >> out_T, "Timestamp GlobalTime(s) Dose(Mrad)"
	#print >> out_T, timestamp + ' ' + globalTime  + ' ' + dose

        print >> out_T, "Start Temperature Measurements" "\n" "REGCONF , SENSOR , ADC Value" "\n"
#        sensor_array=['RADSENS_4']
#
        sensor_array=['TEMPSENS_1','TEMPSENS_2','TEMPSENS_3','TEMPSENS_4','RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']
        raw_adc = []
#        sensor_array=['RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']

# For each sensor, set SENS_SEL_BIAS to 0 and do ADC conversion for SENS_DEM varying from 0 to 15

        for sensor in sensor_array:
          for s_config in config_array:
            for DEM in range (0,16):
                REGCONF = (s_config | DEM << 1 )
                self.set_monitor(REGCONF)


                # print ("s_config is %d" %(s_config))
                # print ("DEM value is %d" % (DEM))
                # print ("REGCONF is %d" % (REGCONF))
                time.sleep (0)
                ADC =  self.get_temperature(sensor)
                raw_adc.append(ADC)

# Serial Communication for Keithley 2270 - Uncomment the following lines to use it
                #ser.write2tty('*RST')
                #ser.write2tty('MEAS:VOLT? 1, 0.000001')
                #out= ser.readFromtty()
                print >> out_T, REGCONF, ADC

                #print REGCONF, ADC


        out_T.close()

        return str(raw_adc)

if __name__ == "__main__":

    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    #ser = serial_com()
    tr=Temperature_readout()
    tr.scan()

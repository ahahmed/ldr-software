import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from matplotlib.pyplot import figure
import basil
import visa
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator ,WeekdayLocator ,DateFormatter
import matplotlib.pyplot as plt
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import matplotlib.dates as mdate
import calendar
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_monitor import meas_temperature
import logging
import matplotlib.pyplot as plt
import re

def list_folders(directory_ringoscillators):
	dir_array = os.listdir(directory_ringoscillators)
	return dir_array

def get_elements_line(x):
	array = []
	for i in range(len(x.split('	'))):
		a = x.split('	')[i]
		if a != '	':
			array.append(a)
			# print array
			# raw_input("a")
	return array




def get_ring_vector(dir_array,directory_ringoscillators):
	r0 = []
	r1 = []
	r2 = []
	r3 = []
	r4 = []
	r5 = []
	r6 = []
	r7 = []
	time = []
	idcount = []
	temperature = []
	global_pulse = 13 #value for global pulse equal to 409.6
	#TODO if global pulse is changed, change also the duration variable
	#global_pulse = 12 #value for global pulse 8 equal to 3276.8e-9
	global_pulse_duration = 3276.8e-9
	# time = 0
	j = 0
	for directory in dir_array:
		idcount.append(j)
		j = j+1
		dir_textfile = directory_ringoscillators + '/' + directory+'/output_data.dat'
		if os.path.exists(dir_textfile):
			f = open(dir_textfile, 'r')
			lines = f.readlines()
			for i, x in enumerate(lines):
				# print i
				# print "'----"
				# print x
				try:
					if i == 1:
						temperature.append((re.findall(r'-?\d+\.?\d*', x)))
					elif i == 2:
						time.append((re.findall(r'-?\d+\.?\d*', x)))
						# raw_input("t")
						#temperature.append('%.1f'%(float(x.split(' ')[0])))
					elif i == global_pulse:
						counts = get_elements_line(x)
						r0.append((float(counts[1]))/(global_pulse_duration))
						r1.append(float(counts[2])/(global_pulse_duration))
						r2.append(float(counts[3])/(global_pulse_duration))
						r3.append(float(counts[4])/(global_pulse_duration))
						r4.append(float(counts[5])/(global_pulse_duration))
						r5.append(float(counts[6])/(global_pulse_duration))
						r6.append(float(counts[7])/(global_pulse_duration))
						r7.append(float(counts[8])/(global_pulse_duration))
						# print r0
				except ValueError,e:
					print "error",e,"on line",i,x
				#print x
				#raw_input("enter1")
				else:
					a = 0

	f.close()
	ring = [r0,r1,r2,r3,r4,r5,r6,r7]
	return ring, idcount, time , counts, temperature



now = time.time()
## Plot
def plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime, temperature, time):

	global_pulse_duration = 204.8e-9


	Hour = HourLocator()   #  Every hour

	day  = DayLocator()  # every day
	week = WeekdayLocator()


	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['R0','R1','R2','R3','R4','R5','R6','R7']
	#figure(figsize=(15, 8))
	#fig, ax = plt.subplots(figsize=(15, 8))
	fig1 = plt.figure()
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax1 = fig1.add_subplot(111)

	for i in range(len(ring)):
			ax.plot_date(datetime, ring[i],'o',label = legend[i])#, label = legend[i],s=0.5, color = line_color[i])

	#now = time.time()
	sec = []
	for i in range(len(time)):
		sec.append((now - time[i])/60/60) 
	ax1.plot(sec,temperature, '-')

	#ax1.set_ylim(35, 45)
	#print time
	#print temperture
	#ax1.plot(time, temperature, '-')#, label = legend[i],s=0.5, color = line_color[i])


	#
	# for i in range(len(temperature)):
	# 		print temperature[i]
	# 		ax1.plot_date(datetime,temperature[i], )#, label = legend[i],s=0.5, color = line_color[i])

	#fig.autofmt_xdate()
	#fig.autoscale_view()
	date_fmt = '%d-%m-%y %H'
	date_fmt = '%d-%m-%y %H'
	#  DateFormatter to set the data to the correct format.
	date_formatter = mdate.DateFormatter(date_fmt)
	ax.xaxis.set_major_formatter(date_formatter)
	# tick labels diagonal so they fit easier.
	fig.autofmt_xdate()
	ax1.set_ylim((-20,50))
	# ax.xaxis.set_major_locator(day)
	# ax.xaxis.set_minor_locator(Hour)
	ax.autoscale_view()

	fig.legend(loc=5)
	plt.title('Ring Oscillators Frequency | global_pulse = 13 | LDO, 1.9 VDDD and VDDA')
	ax.set_xlabel('Time')
	ax.set_ylabel('Frequency [hz]')
	ax1.set_ylabel('Temperture')
	fig.savefig(directory_plots+'/'+'_ringOscillators_freq_last_plot.png', dpi=1000)
	fig1.savefig(directory_plots+'/'+'_ringOscillators_TEMP_last_plot.png', dpi=1000)
	#ax1.set_ylim(0, 100)
	#ax.set_ylim(-20,100)
	#plt.show()
	#ax.set_ylim(-20,100)
	#plt.pause(100000)
	#plt.close()


# def save_RO_data_T(dir,counts):
# 	NTC = meas_temperature()
# 	counts.insert(0, time.time())
# 	with open(dir + '/' + 'RO.txt', 'a') as output:
# 		output.write(','.join(map(repr, counts))
# 		output.write("\n")








if __name__ == "__main__": 
    '''
trimvoltage_dict = "Vtrim_digital_optimal"
trimvoltage_dict = "Vtrim_digital_+Onebit"
trimvoltage_dict = "Vtrim_digital_+twobits"
trimvoltage_dict = "Vtrim_digital_+Threebits"
trimvoltage_dict = "Vtrim_digital_-Onebit"
trimvoltage_dict = "Vtrim_digital_-twobit"

    '''

    directory = '00_TEST_INFO'
    if not os.path.exists(directory):
	os.makedirs(directory)
    '''
    directory_plots = '00_TEST_INFO/ring_oscillator_processed '
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    '''

    "1"		

    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_optimal'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_optimal'
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
    
    
    "2"
    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_+Onebit'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_+Onebit'
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
    
    
    
    
    
    
    "3"
    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_+twobits'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_+twobits'
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
    
    
    
    
    
    "4"
    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_+threebits'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_+threebits'
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
    
    
    

    "5"
    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_-Onebit'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_-Onebit'
    if not os.path.exists(directory_plots):
    	os.makedirs(directory_plots)  
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
    
    
    
    
    "6"
    directory_ringoscillators = '00_TEST_INFO/ring_oscillators/Vtrim_digital_-twobit'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/Vtrim_digital_-twobit'
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    for i in range(0,len(time)):
    	datetime.append(mdate.epoch2num(float(time[i][0])))
    for i in range(0,len(time)):
    	timeplot.append((float(time[i][0])))
    temp = []
    for x in temperature:
    	for y in x:
    		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, datetime,temp, timeplot )
 
    
    
    
    
    
    
    
    
    
    
    

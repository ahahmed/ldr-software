import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select
import shutil
from shutil import copytree, ignore_patterns

import fnmatch
import os
import re



#
#
def removecontent(path):
	if os.path.exists(path):
		try:
			os.system("rm -r "+ path )
		except Exception as e:
			pass


#It goes through all the defaul_chip files generated
def ChipCharact():
	directory = '00_TEST_INFO'+ '/' +'Chip_Characterization'
	currentdir = os.getcwd()
	if not os.path.exists(directory):
    		os.makedirs(directory)


	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	testTag = ('Full_Characterization ')
	#testTag = 'irradiation_'

	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	if not os.path.exists(directory):
		os.makedirs(directory)





	# All FE tests:
	try:
		removecontent("output_data")
		os.system("bdaq53 scan_digital")
		shutil.copytree("output_data", directory + '/all_digital_scan', ignore=ignore_patterns('*.log'))
		removecontent("output_data")
		os.system("bdaq53 scan_analog")
		shutil.copytree("output_data", directory + '/all_analog_scan', ignore=ignore_patterns('*.log'))

	except Exception as e:
		raise
	finally:

		# power cycle
		pass

	#Sync FE:
	try:
		removecontent("output_data")
		os.system("bdaq53 scan_noise_occupancy_sync")
		os.system("bdaq53 scan_analog_sync")
		shutil.copytree("output_data", directory + '/sync_analog_scan', ignore=ignore_patterns('*.log'))

		# Tuning the front end with auto zeoing, and width of 6, 7 8 and 9. which are saved in their resepctive folders.
		removecontent("output_data")
		os.system("bdaq53 scan_noise_occupancy_sync")
		os.system("bdaq53 scan_threshold_width6")
		shutil.copytree("output_data", directory + '/sync_threshold_sync_scan_width6', ignore=ignore_patterns('*.log'))
		os.system("bdaq53 scan_threshold_width7")
		shutil.copytree("output_data", directory + '/sync_threshold_sync_scan_width7', ignore=ignore_patterns('*.log'))
		os.system("bdaq53 scan_threshold_width8")
		shutil.copytree("output_data", directory + '/sync_threshold_sync_scan_width8', ignore=ignore_patterns('*.log'))
		os.system("bdaq53 scan_threshold_width9")
		shutil.copytree("output_data", directory + '/sync_threshold_sync_scan_width9', ignore=ignore_patterns('*.log'))

	except Exception as e:
		raise
	finally:
		pass



	#Lin FE:
	try:
		removecontent("output_data")
		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 scan_analog_lin")
		shutil.copytree("output_data", directory + '/lin_analog_scan', ignore=ignore_patterns('*.log'))
		removecontent("output_data")

		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 scan_threshold_lin")
		shutil.copytree("output_data", directory + '/lin_threshold_scan_lin_untuned', ignore=ignore_patterns('*.log'))
		removecontent("output_data")
	# 	os.system("mv output_data "+ directory + '/lin_threshold_scan_lin_untuned')

		if not os.path.isfile("lin_Tdac_metatune_mask" + ".h5"):
			removecontent("output_data")
			os.system("bdaq53 scan_noise_occupancy_lin")
			os.system("bdaq53 meta_tune_local_threshold_lin_ini")
			for file in os.listdir('output_data'):
				if file.endswith("mask.h5"):
					shutil.copy("output_data/" +file, "lin_Tdac_metatune_mask" + ".h5")
			shutil.move("output_data", directory + '/lin_meta_tune_threshold_initial')



			'''
			using linear triming TDACs before from previous run for a threshodls scan:
			'''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")
		shutil.copy("lin_Tdac_metatune_mask" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 meta_tune_local_threshold_lin")
		for file in os.listdir('output_data'):
			if file.endswith("mask.h5"):
				shutil.copy("output_data/" +file, "lin_Tdac_metatune_mask" + ".h5")
		shutil.move("output_data", directory + '/lin_meta_tune_threshold')
	except Exception as e:
		raise
	finally:
		pass



	#Diff FE:
	try:
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_analog_diff")
		shutil.copytree("output_data", directory + '/diff_analog_scan_diff')


		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		shutil.copytree("output_data", directory + '/diff_threshold_scan_untuned')


		''' Inital tuning. this will only run first time. '''
		TDACS = []
 		if not os.path.isfile("diff_Tdac_metatune_mask" + ".h5"):
			os.system("bdaq53 scan_noise_occupancy_diff")
			os.system("bdaq53 meta_tune_local_threshold_diff_ini")
			for file in os.listdir('output_data'):
				if file.endswith("mask.h5"):
					TDACS.append(file)
			for file in TDACS:
				if os.path.getsize('output_data/' + file) > 10000: # if the file is bigger than 10000 bytes, its the TDAC mask file for tuning.
					shutil.copy("output_data/" +file, "diff_Tdac_metatune_mask" + ".h5")
			shutil.move("output_data", directory + '/diff_meta_tune_threshold_initial')

		''' using linear triming TDACs before irradiation for a threshold scan: '''
		TDACS = []
		if not os.path.exists('output_data'):
			os.makedirs("output_data")
		shutil.copy("diff_Tdac_metatune_mask" + ".h5", "output_data")
		# os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 meta_tune_local_threshold_diff")
		for file in os.listdir('output_data'):
			if file.endswith("mask.h5"):
				TDACS.append(file)
		for file in TDACS:
			if os.path.getsize('output_data/' + file) > 10000: # if the file is bigger than 10000 bytes, its the TDAC mask file for tuning.
				shutil.copy("output_data/" +file, "diff_Tdac_metatune_mask" + ".h5")
		shutil.move("output_data", directory + '/diff_meta_tune_threshold')


	except Exception as e:
		raise
	finally:
		pass
	#
	# tend = time.time()
	# print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	 ChipCharact()

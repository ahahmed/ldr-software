#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''

import numpy
import yaml
import time
import os
import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils
from bitarray import bitarray
from datetime import datetime
import matplotlib
import matplotlib.pyplot as plt
#from mon1 import monitor_datalogger_power_supply
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import collections


trimvoltage_dict = collections.OrderedDict()
trimvoltage_dict["Vtrim_digital_optimal"] = 0b1011110110
trimvoltage_dict["Vtrim_digital_+Onebit"] = 0b1011110111
trimvoltage_dict["Vtrim_digital_+twobits"]= 0b1011111000
trimvoltage_dict["Vtrim_digital_+Threebits"] = 0b1011111001
trimvoltage_dict["Vtrim_digital_-Onebit"] = 0b1011110101
trimvoltage_dict["Vtrim_digital_-twobit"] = 0b1011110100


chip_configuration = 'default_chip.yaml'

def removecontent(path):
	if os.path.exists(path):
		try:
			os.system("rm -r "+ path )
		except Exception as e:
			pass

local_configuration = {
    # Hardware settings
    'VDDA'              : 1.27,
    'VDDD'              : 1.27,
    'VDDA_cur_lim'      : 0.9,
    'VDDD_cur_lim'      : 0.9,
}

#legend_ntc_power = ' T_NTC(C) VDDA-TO VDDD-TO VDDA-TO-WC VDDD-TO-WC VDDA-BGPV VDDD-BGPV VDDA-BGPV-WC VDDD-BGPV-WC VDDD-LBNL VDDA-LBNL VDDD-LBNL-WC VDDA-LBNL-WC Idig Iana'

legend_ntc_power = ' T_NTC(C) 	GlobalTime(s)  DoseRange(Mrad)'

class scan_ring_oscillator_optimal(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_optimal"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):




	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_optimal'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''

			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_optimal.analyze()

        self.logger.info('Scan finished')

    #
	# plt.figure(1, figsize = (10,8))
	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator linearity')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Measured counts')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
    #
    #
	# plt.figure(2, figsize = (10,8))
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator frequency')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Frequency, [GHz]')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
    #
    #



	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

	#Temperature measurements
	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
	'''
	#Power measurements
	powerMeasurements = monitor_datalogger_power_supply()
	for item in powerMeasurements[0]:
		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)

            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass

	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()

class scan_ring_oscillator_optimal_add1bit(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal_add1bit, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_+Onebit"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):




	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_+Onebit'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''

			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_add1bit.analyze()

        self.logger.info('Scan finished')


    	# plt.figure(1, figsize = (10,8))
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
    	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
    	# plt.legend()
        # plt.title('Oscillator linearity')
        # plt.xlabel('Global pulse duration, [ns]')
        # plt.ylabel('Measured counts')
    	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
        #
        #
    	# plt.figure(2, figsize = (10,8))
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
    	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
    	# plt.legend()
        # plt.title('Oscillator frequency')
        # plt.xlabel('Global pulse duration, [ns]')
        # plt.ylabel('Frequency, [GHz]')
    	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
        #
        #
        #


    	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
    	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

    	#Temperature measurements
    	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
    	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
    	'''
    	#Power measurements
    	powerMeasurements = monitor_datalogger_power_supply()
    	for item in powerMeasurements[0]:
    		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
    	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
    	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)
            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass


	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()


class scan_ring_oscillator_optimal_add2bit(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal_add2bit, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_+twobits"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_+twobits'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''





			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_add2bit.analyze()

        self.logger.info('Scan finished')

    #
	# plt.figure(1, figsize = (10,8))
	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator linearity')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Measured counts')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
    #
    #
	# plt.figure(2, figsize = (10,8))
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator frequency')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Frequency, [GHz]')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
    #




	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

	#Temperature measurements
	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
	'''
	#Power measurements
	powerMeasurements = monitor_datalogger_power_supply()
	for item in powerMeasurements[0]:
		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)
            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass


	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()


class scan_ring_oscillator_optimal_add3bit(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal_add3bit, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_+Threebits"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_+threebits'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''





			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_add3bit.analyze()

        self.logger.info('Scan finished')

    #
	# plt.figure(1, figsize = (10,8))
	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator linearity')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Measured counts')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
    #
    #
	# plt.figure(2, figsize = (10,8))
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator frequency')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Frequency, [GHz]')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
    #




	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

	#Temperature measurements
	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
	'''
	#Power measurements
	powerMeasurements = monitor_datalogger_power_supply()
	for item in powerMeasurements[0]:
		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)
            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass


	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()








class scan_ring_oscillator_optimal_sub1bit(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal_sub1bit, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_-Onebit"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_-Onebit'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''





			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_sub1bit.analyze()

        self.logger.info('Scan finished')

    #
	# plt.figure(1, figsize = (10,8))
	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator linearity')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Measured counts')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
    #
    #
	# plt.figure(2, figsize = (10,8))
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator frequency')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Frequency, [GHz]')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
    #




	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

	#Temperature measurements
	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
	'''
	#Power measurements
	powerMeasurements = monitor_datalogger_power_supply()
	for item in powerMeasurements[0]:
		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)
            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass


	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()







class scan_ring_oscillator_optimal_sub2bit(ScanBase):
    scan_id = "scan_ring_oscillator"
    def configure(self, **kwargs):
        super(scan_ring_oscillator_optimal_sub2bit, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
        self.chip.write_register('VOLTAGE_TRIM', data=trimvoltage_dict["Vtrim_digital_-twobit"]) #fine for Vdda = 1.2 and VDDD = 1.2

    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
	self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  **kwargs):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ring_oscillators/Vtrim_digital_-twobit'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)


	self.folder_name = directory +"/Run" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	os.mkdir(self.folder_name)
	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33'];
    #print ("point 2")
        self.count1 = {}
        self.count2 = {}
        self.oscilator_counts = numpy.zeros((10, 8))
	pulse_duration = numpy.zeros((10, 1))

        self.logger.info('Starting scan...')

	indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
	self.chip.write_command(indata)
	for self.pulse_width in range(0, 10):
		pulse_duration[self.pulse_width] = 2**(self.pulse_width)*6.4
		with self.readout(fill_buffer=True, clear_buffer=False):
			self.chip.write_register(register=109, data=0x00, write=True)
			self.chip.write_register(register=109, data=0xFF, write=True)
			self.write_ring_oscillator(value = 0)
			time.sleep(0.5)


			indata = self.write_global_pulse(width=self.pulse_width, write=False)
			self.chip.write_command(indata)
			self.read_ring_oscillator()
			data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")



			#Temperature measurements
			Temperature = self.chip._measure_temperature_ntc_CERNFMC()
			tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature) + ' '

			'''
			#Power measurements
			powerMeasurements = monitor_datalogger_power_supply()
			for item in powerMeasurements[0]:
				tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
			'''





			data_file.write( "\n" + "Run " + str(datetime.now()) + "\n"+"pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns"+ "\n"+ tobewritten +  "\n" )

			data_file.close()
			self.logger.info("pulse width = "+ str(self.pulse_width) + " --> "+ str(pulse_duration[self.pulse_width]) + " ns ")
		scan_sub2bit.analyze()

        self.logger.info('Scan finished')

    #
	# plt.figure(1, figsize = (10,8))
	# plt.loglog(pulse_duration, self.oscilator_counts[:,0], color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,1], color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,2], color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,3], color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,4], color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,5], color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,6], color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.loglog(pulse_duration, self.oscilator_counts[:,7], color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator linearity')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Measured counts')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_counts.png")
    #
    #
	# plt.figure(2, figsize = (10,8))
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,0], pulse_duration[:,0]), color=line_color[0], label='RING_OSC_0', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,1], pulse_duration[:,0]), color=line_color[1], label='RING_OSC_1', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,2], pulse_duration[:,0]), color=line_color[2], label='RING_OSC_2', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,3], pulse_duration[:,0]), color=line_color[3], label='RING_OSC_3', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,4], pulse_duration[:,0]), color=line_color[4], label='RING_OSC_4', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,5], pulse_duration[:,0]), color=line_color[5], label='RING_OSC_5', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,6], pulse_duration[:,0]), color=line_color[6], label='RING_OSC_6', marker = 's', markersize=6)
	# plt.semilogx(pulse_duration, numpy.divide(self.oscilator_counts[:,7], pulse_duration[:,0]), color=line_color[7], label='RING_OSC_7', marker = 's', markersize=6)
	# plt.legend()
    #     plt.title('Oscillator frequency')
    #     plt.xlabel('Global pulse duration, [ns]')
    #     plt.ylabel('Frequency, [GHz]')
	# plt.savefig(os.getcwd() + "/" + self.folder_name + "/Oscillator_frequency.png")
    #




	self.oscilator_counts = numpy.hstack((pulse_duration, self.oscilator_counts))
	data_file = open(os.getcwd() + "/" + self.folder_name + "/output_data.dat","a")

	#Temperature measurements
	Temperature = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten = legend_ntc_power + "\n" + str("%.2f" %Temperature)
	'''
	#Power measurements
	powerMeasurements = monitor_datalogger_power_supply()
	for item in powerMeasurements[0]:
		tobewritten = tobewritten + ' ' + str("%.3f" %abs(item))
	'''
	data_file.write( tobewritten + " \n")


        data_file.write(str(time.time()))
        data_file.write("\n")
        data_file.write("GlobDur	R0	R1	R2	R3	R4	R5	R6	R7 \n")
        col_maxes = [max([len(("{:"+"g"+"}").format(x)) for x in col]) for col in self.oscilator_counts.T]
	self.pulse_width = 0
        for x in self.oscilator_counts:
               for i, y in enumerate(x):
                       data_file.write(("{:"+str(col_maxes[i])+"g"+"}").format(y)+"	")
               data_file.write("\n")
        data_file.close()

	#plt.show()
    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.count1[elements[1]] = elements[2] >> 12
                self.count2[elements[1]] = elements[2] & ((1 << 12) - 1)
            try:
                self.oscilator_counts[self.pulse_width,:] = [self.count2['RING_OSC_0'], self.count2['RING_OSC_1'], self.count2['RING_OSC_2'], self.count2['RING_OSC_3'], self.count2['RING_OSC_4'], self.count2['RING_OSC_5'], self.count2['RING_OSC_6'], self.count2['RING_OSC_7']]
            except:
                pass


	    data_file = open(os.getcwd() + "/" + self.folder_name + "/all_data.dat","a")
	    data_file.write(str(self.count1) + "\n")
	    data_file.write(str(self.count2) + "\n")
            data_file.close()







if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)


    scan_optimal = scan_ring_oscillator_optimal()
    scan_optimal.start(**configuration)
    scan_optimal.close()
    removecontent("output_data")

    scan_add1bit = scan_ring_oscillator_optimal_add1bit()
    scan_add1bit.start(**configuration)
    scan_add1bit.close()
    removecontent("output_data")

    scan_add2bit = scan_ring_oscillator_optimal_add2bit()
    scan_add2bit.start(**configuration)
    scan_add2bit.close()
    removecontent("output_data")

    scan_add3bit = scan_ring_oscillator_optimal_add3bit()
    scan_add3bit.start(**configuration)
    scan_add3bit.close()
    removecontent("output_data")
    
    
    
    scan_sub1bit = scan_ring_oscillator_optimal_sub1bit()
    scan_sub1bit.start(**configuration)
    scan_sub1bit.close()
    removecontent("output_data")
    
    scan_sub2bit = scan_ring_oscillator_optimal_sub2bit()
    scan_sub2bit.start(**configuration)
    scan_sub2bit.close()
    removecontent("output_data")

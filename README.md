mkdir projectdir
cd projectdir
curl https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -o miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda

gedit ~/.bashrc
----add this line to gedit: export PATH=$HOME/miniconda/bin:$PATH
export PATH=$HOME/miniconda/bin:$PATH

conda update --yes conda
conda install --yes numpy bitarray pytest pyyaml progressbar scipy numba pytables matplotlib tqdm
pip install cocotb
pip install pytemperature
pip install pyvisa

git clone https://gitlab.cern.ch/ahahmed/test_software_ldr.git
cd test_software_ldr/

git clone -b v2.4.13 https://github.com/SiLab-Bonn/basil
cd basil
python setup.py develop
cd ..

git clone -b v0.9.0 https://gitlab.cern.ch/silab/bdaq53.git
cd bdaq53
python setup.py develop
git checkout -b development

sudo ifconfig em1 192.168.10.2

bdaq53 scan_digital


mailx instructions - https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8

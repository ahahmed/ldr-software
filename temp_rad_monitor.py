from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_readout import Temperature_readout
import matplotlib.dates as mdate
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator , DateFormatter
import matplotlib.pyplot as plt
#from bdaq53.serial_com import serial_com
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import pytemperature
import time
import os
import math
import datetime
import sys
import calendar
import numpy as np
import logging
import yaml
import time
import re
import csv
from matplotlib import pyplot as plt
from Ldr_Temp_monitor import meas_temperature
from matplotlib import dates
from matplotlib.dates import AutoDateFormatter, AutoDateLocator
# variables/lists
raw_adc = []
averages = []
Sensors  = []
Filterout_numbers = [1,2,3,4]# for filtering out umwanted char
SenseArraysize = 16
d_config = 0

class Temperature_readout():
    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
 #       self.set_monitor()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array

    def set_monitor(self, d_config=0):

########       set SENSOR_CONFIG_X default values - the same config for the 4 sensors
        self.chip.write_register(register='SENSOR_CONFIG_0', data=(d_config << 6|d_config), write=True)
        self.chip.write_register(register='SENSOR_CONFIG_1', data=(d_config << 6|d_config), write=True)

    def get_temperature(self,sensor):
        temperatures={}
        try:
            timeout=10000

            self.chip.get_ADC(typ='U' ,address=sensor)

            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        temperatures[sensor] = userk_data['Data'][0]
                        return temperatures
            else:
                logging.error('Timeout while waiting for chip Temperature.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
	SENS_CONF_L  = 0b100000
	SENS_CONF_H  = 0b100001
        config_array=[SENS_CONF_L,SENS_CONF_H]
	s_config = 0
	DEM = 0
	CONFIG = 0
########       set MON_ENABLE = 1 (MONITOR_CONFIG[11])
########       set MON_BG_TRIM=10000 (MONITOR_CONFIG[10:6])
########       set MON_ADC_TRIM = 000101 (MONITOR_CONFIG[5:0])
        self.chip.write_register(register='MONITOR_CONFIG', data=0b110000000101, write=True)
        sensor_array=['TEMPSENS_1','TEMPSENS_2','TEMPSENS_3','TEMPSENS_4','RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']
        raw_adc = []


	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	directory = '00_TEST_INFO/temperature_sensor/temp_data_backup'
	if not os.path.exists(directory):
    		os.makedirs(directory)
		out_T = open( directory + '/' + timestamp + "_temperature_sensor.txt", "a")
		out_T.close()

	directory_textfile = directory + '/' + timestamp + "_temperature_sensor.txt"
	directory_notextfile = directory
	out_T = open( directory + '/' + timestamp + "_temperature_sensor.txt", "a")
	now = datetime.datetime.now()
        out_T = open(directory_textfile, "a+")

	print >> out_T, "Timestamp GlobalTime(s) Dose(Mrad)"
	#print >> out_T, timestamp + ' ' + globalTime  + ' ' + dose

        print >> out_T, "Start Temperature Measurements" "\n" "REGCONF , SENSOR , ADC Value" "\n"
#        sensor_array=['RADSENS_4']
#
        sensor_array=['TEMPSENS_1','TEMPSENS_2','TEMPSENS_3','TEMPSENS_4','RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']
        raw_adc = []
#        sensor_array=['RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']

# For each sensor, set SENS_SEL_BIAS to 0 and do ADC conversion for SENS_DEM varying from 0 to 15

        for sensor in sensor_array:
          for s_config in config_array:
            for DEM in range (0,16):
                REGCONF = (s_config | DEM << 1 )
                self.set_monitor(REGCONF)
                # print ("s_config is %d" %(s_config))
                # print ("DEM value is %d" % (DEM))
                # print ("REGCONF is %d" % (REGCONF))
                time.sleep (0.2)
                ADC =  self.get_temperature(sensor)
                print ADC
                raw_adc.append(ADC)
                print >> out_T, REGCONF, ADC
                #print REGCONF, ADC
        out_T.close()

# Serial Communication for Keithley 2270 - Uncomment the following lines to use it
                #ser.write2tty('*RST')
                #ser.write2tty('MEAS:VOLT? 1, 0.000001')
                #out= ser.readFromtty()
        return str(raw_adc)


    def DeltaV_temperature(self, raw_adc):
        raw_adc_buffer = []
        raw_adc_buffer.append(re.findall(r'-?\d+\.?\d*', raw_adc)); raw_adc_buffer = raw_adc_buffer[0] # remove letter
        raw_adc_buffer = map(int, raw_adc_buffer) # find average of alle the ADC for sens 1
        for i in raw_adc_buffer:
            if i in Filterout_numbers:
                raw_adc_buffer.remove(i) # remove all 1,2,3,4
            chunks = [raw_adc_buffer[x:x+16] for x in xrange(0, len(raw_adc_buffer), 16)] # Devide the raw adc values to chunks of 16. Each sensor consis of 32 adc values
        for sublist in chunks: # avarage for the dynamic element method, length of the list = 16
            averages.append(sum(sublist)/float(len(sublist)))
            k = 0
            j = 1
        for i in range(len(averages)-1):# finding DeltaCode for every sensor, lenth of the list = 8
            while k|j < len(averages):
                Sensors.append(averages[j]-averages[k])
                k = k+2
                j = j+2
        return Sensors


if __name__ == "__main__":
    #NTC measurements for calibration
    NTC = meas_temperature()
    TempLDR_instance = Temperature_readout()
    raw_adc = TempLDR_instance.scan()
    sensors = TempLDR_instance.DeltaV_temperature(raw_adc)

    # Calibration parameters
    k = 1.38064852*math.pow(10,-23) #
    q = 1.60217662*math.pow(10,-19) #
    V_refADC_SCC203 = 0.911 #[V]    #
    LSB = V_refADC_SCC203/math.pow(2, 12) #
    R = np.log(15)
    NTC_absolut = pytemperature.c2k(NTC)
    constant = 4285.2

    # # Calculation Nf for each sensor
    Nf_sensors = [(i * LSB)/(((k*NTC_absolut)/(q))*R) for i in sensors]
    sensors.insert(0,NTC)
    sensors.insert(0, time.time())
    directory = '00_TEST_INFO/temperature_sensor/processed_data'
    if not os.path.exists(directory):
        os.makedirs(directory)
    outputfile_DeltaV = open(directory +'/' + "temperature_sensor_delta_V.txt", "a")
    inputfile_DeltaV = (directory +'/' + "temperature_sensor_delta_V.txt")


    outputfile_DeltaV.write(', '.join(map(repr, sensors)) +	'\n')
    outputfile_DeltaV.close()

    # with open(inputfile_DeltaV) as f:
    #     DeltaV_lines = f.read().splitlines()
    # print DeltaV_lines
    #
    # print [x.split(' ')[1] for x in open(file).readlines()]
    f=open(inputfile_DeltaV,"r")
    lines=f.readlines()
    Time = []
    NTC_T = []
    Tsens1, Tsens2, Tsens3, Tsens4, Rsens1, Rsens2, Rsens3, Rsens4 = ([] for i in range(8))
    for x in lines:

        Time.append(x.split(',')[0]); NTC_T.append(x.split(',')[1])
        Tsens1.append(x.split(',')[2]); Tsens2.append(x.split(',')[3])
        Tsens3.append(x.split(',')[4]); Tsens4.append(x.split(',')[5])
        Rsens1.append(x.split(',')[6]);Rsens2.append(x.split(',')[7])
        Rsens3.append(x.split(',')[8]); Rsens4.append(x.split(',')[9])
    f.close()

    Time = map(float, Time); NTC_T = map(float, NTC_T)
    Tsens1 = map(float, Tsens1); Tsens2 = map(float, Tsens2)
    Tsens3 = map(float, Tsens3); Tsens4 = map(float, Tsens4)
    Rsens1 = map(float, Rsens1); Rsens2 = map(float, Rsens2)
    Rsens3 = map(float, Rsens3); Rsens4 = map(float, Rsens4)
    test = Nf_sensors[0]


    Tsens1 = np.array(pytemperature.k2c((constant/Nf_sensors[0])*LSB*np.array(Tsens1))); Tsens2 = pytemperature.k2c((constant/Nf_sensors[1])*LSB*np.array(Tsens2))
    Tsens3 = pytemperature.k2c((constant/Nf_sensors[2])*LSB*np.array(Tsens3)); Tsens4 = pytemperature.k2c((constant/Nf_sensors[3])*LSB*np.array(Tsens4))
    Rsens1 = pytemperature.k2c((constant/Nf_sensors[4])*LSB*np.array(Rsens1)); Rsens2 = pytemperature.k2c((constant/Nf_sensors[5])*LSB*np.array(Rsens2))
    Rsens3 = pytemperature.k2c((constant/Nf_sensors[6])*LSB*np.array(Rsens3)); Rsens4 = pytemperature.k2c((constant/Nf_sensors[7])*LSB*np.array(Rsens4))



    # convert time to datetime
    Datetime = []
    for val in Time:
        Datetime.append(mdate.epoch2num(val))

    temprad = [Tsens1, Tsens2, Tsens3, Tsens4,Rsens1,Rsens2,Rsens3,Rsens4]
    legendtemp = ['tempsens1','tempsens2','tempsens3','tempsens4','radsens1','radsens2','radsens3','radsens4']
    #ax1 = fig.add_subplot(111)


    locator = AutoDateLocator()
    formatter = AutoDateFormatter(locator)


    fig, (ax1) = plt.subplots( figsize=(15, 8))
    for i in range(len(temprad)):
        ax1.plot_date(Datetime,temprad[i],'o',label = legendtemp[i])#, label = legend[i],s=0.5, color = line_color[i])
        ax1.xaxis.set_major_locator(locator)
        ax1.xaxis.set_major_formatter(formatter)
    ax1.plot_date(Datetime,NTC_T,'k-',label = "NTC", linewidth = 4.0) #, label = legend[i],s=0.5, color = line_color[i])
    date_fmt = '%d-%m-%H-%M'
    ax1.set_ylim((32,42))

    ax1.autoscale_view()
    fig.autofmt_xdate()
    ax1.legend(loc=4)
    ax1.set_title('All 8 temp/rad sensors + NTC | Calibration temp: auto')
    ax1.set_xlabel('Time [Auto]')
    ax1.set_ylabel('Temperature (C)')

    fig.subplots_adjust(hspace=0)
	#plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
    plt.grid(b=None, which='minor', axis='both')
    plt.savefig(directory +'/' + "AllSensor_last_update.PNG", dpi=1000)
    plt.show(block=False)
    plt.pause(10)
    plt.close()




    #Temperature_sensors = [(constant/i)*(LSB*np.array(tempsen for i in sensors]
